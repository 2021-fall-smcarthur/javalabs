package com.oreillyauto.w5.d2;

import java.util.Arrays;

/**
 * ChessPiece abbreviations should be one of the following: BR - Black Rook WR - White Rook BN - Black Knight WN - White Knight BB - Black
 * Bishop WB - White Bishop BK - Black King WK - White King BQ - Black Queen WQ - White Queen BP - Black Pawn WP - White Pawn
 * 
 * @author jbrannon5
 */
public class ChessBoard {


    public ChessBoard() {
    }

  
    public ChessPiece[][] buildChessBoard() {
        ChessPiece[][] boardArray = new ChessPiece[9][10];
        ChessPiece[] chessPieces = new ChessPiece[32];
        String color = "B";
        String type = "P";
        for (int i = 0; i < 32; i++) {

            if (i > 15) {
                color = "W";
            }
            switch (i) {
                case 0:
                case 7:
                case 24:
                case 31:
                    type = "R";
                    break;
                case 1:
                case 6:
                case 25:
                case 30:
                    type = "N";
                    break;
                case 2:
                case 5:
                case 26:
                case 29:
                    type = "B";
                    break;
                case 4:
                case 28:
                    type = "K";
                    break;
                case 3:
                case 27:
                    type = "Q";
                    break;
                default:
                    type = "P";
                    break;

            }
            chessPieces[i] = new ChessPiece(type, color, false);
        }
        // boardArray[0][0] = " ";
        int chessPieceArrayIndex = 0;
        //char intToChar = 'a';
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (i == 0 || i == 1 || i == 6 || i == 7) {

                    boardArray[i][j] = chessPieces[chessPieceArrayIndex++];

                    //System.out.print(boardArray[i][j]);
                    // System.out.print(" ");
                } else {
                    boardArray[i][j] = new ChessPiece("-", "-", false);
                    //System.out.print(boardArray[i][j]);
                    //System.out.print(" ");
                }

            }
        }
        // System.out.print('\n');
        boardArray[0][9]=new ChessPiece("W","-", false);
        boardArray[1][9] = new ChessPiece("-","-", false);
        boardArray[2][9] =new ChessPiece("-","-", false);
        return boardArray;
    }

    public ChessPiece[][] movePiece(ChessPiece[][] chessBoard, String pieceToMoveStart, String pieceToMoveEnd, String turn) {
        int startRowIndex = ((int) pieceToMoveStart.charAt(1) - 49);
        int startColIndex = (int) pieceToMoveStart.charAt(0) - 65;

        int endRowIndex = ((int) pieceToMoveEnd.charAt(1) - 49);
        int endColIndex = (int) pieceToMoveEnd.charAt(0) - 65;
        String checkEnd = String.valueOf(endRowIndex) + String.valueOf(endColIndex);

        System.out.print("Moving piece at: " + startRowIndex + startColIndex + " to: " + endRowIndex + endColIndex + "\n");

        boolean canMove = false;
        if (chessBoard[startRowIndex][startColIndex].getColor().equals(turn)) {

            ChessPiece pieceToMove = chessBoard[startRowIndex][startColIndex];
            String[] moveArray = pieceToMove.findLegalMoves(pieceToMove, chessBoard, startRowIndex, startColIndex);
            //System.out.print("moveArrayLength: "+moveArray.length+"\n");
            for (int i = 0; i < moveArray.length; i++) {
                System.out.print("moveArray[i]: " + moveArray[i] + "\n");
                if (checkEnd.equals(moveArray[i])) {
                    canMove = true;
                    break;
                }
            }
            if (canMove) {
                ChessPiece removedPiece = chessBoard[endRowIndex][endColIndex];
                chessBoard[startRowIndex][startColIndex] = new ChessPiece("-", "-", false);
                chessBoard[endRowIndex][endColIndex]= pieceToMove;
                if (parseForCheck(chessBoard, turn)) {
                    canMove = false;
                    chessBoard[endRowIndex][endColIndex]=removedPiece;
                    chessBoard[startRowIndex][startColIndex]=pieceToMove;
                    //chessBoard[1][8]= new ChessPiece("-","-",false);
                    //chessBoard[2][8] = new ChessPiece("-","-", false);
                    showBoard(chessBoard);
                    System.out.print("That move will put you in check!\n");
                    return chessBoard;
                }
            }else {
                System.out.print("Illegal move\n");
                showBoard(chessBoard);
                return chessBoard;
            }

            System.out.print("Can Move: " + canMove + "\n");
            //System.out.print(pieceToMove.getAbbreviation());
            chessBoard[startRowIndex][startColIndex] = new ChessPiece("-", "-", false);
            //chessBoard[endRowIndex][endColIndex] = new ChessPiece(pieceToMove.getType(),pieceToMove.getColor());
            pieceToMove.setMoved(true);
            chessBoard[endRowIndex][endColIndex] = pieceToMove;
            String nextTurn = null;
            if (turn.equals("W")) {
                nextTurn = "B";
            }else {
                nextTurn = "W";
            }
            if (parseForCheck(chessBoard, nextTurn)) {
                if(parseForCheckMate(chessBoard, nextTurn)){
                    chessBoard[2][9] = new ChessPiece("-",turn,false);
                }
                chessBoard[1][9]= new ChessPiece("K",nextTurn,false);
                
            }
            else {
                chessBoard[1][9] = new ChessPiece("-", "-", false);
            }
            chessBoard[0][9].setColor(nextTurn);
        } else {
            System.out.print("Thats not your color!");
            showBoard(chessBoard);
        }
        showBoard(chessBoard);
        return chessBoard;
    }

    public static boolean parseForCheck(ChessPiece[][] chessBoard, String turn) {
        ChessPiece currentPiece = null;
        String[] legalMoves = new String[0];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                currentPiece = chessBoard[i][j];
                legalMoves = currentPiece.findLegalMoves(currentPiece, chessBoard, i, j);
                for (int x = 0; x < legalMoves.length; x++) {
                    int boardRow = legalMoves[x].charAt(0) - 48;
                    int boardCol = legalMoves[x].charAt(1) - 48;
                    if (boardRow >= 0 && boardRow < 8 && boardCol >= 0 && boardCol < 8) {
                        //System.out.print("Check index: "+boardRow+boardCol+currentPiece.getAbbreviation()+"\n");
                        if (chessBoard[boardRow][boardCol].getAbbreviation().equals(turn+"K")) {
                           
                            //chessBoard.setCheck(checkColor);
                            return true;
                        }
                    }

                }
            }
        }
        return false;
    }
    
    public static boolean parseForCheckMate(ChessPiece[][] chessBoard, String turn) {
        for (int i = 0; i <= 7; i++) {
            for (int j = 0; j <= 7; j++) {
                if (chessBoard[i][j].getColor()==turn) {
                    String[] moveList = chessBoard[i][j].findLegalMoves(chessBoard[i][j], chessBoard, i, j);
                    ChessPiece pieceToMove = chessBoard[i][j];
                    for (int x = 0; x < moveList.length; x++) {
                        int boardRow = moveList[x].charAt(0) - 48;
                        int boardCol = moveList[x].charAt(1) - 48;
                        ChessPiece removedPiece = chessBoard[boardRow][boardCol];
                        chessBoard[boardRow][boardCol] = pieceToMove;
                        chessBoard[i][j] = new ChessPiece("-","-",false);
                        if (!(parseForCheck(chessBoard, turn))){
                            chessBoard[boardRow][boardCol] = removedPiece;
                            chessBoard[i][j] = pieceToMove;
                            
                            return false;
                        }
                        chessBoard[boardRow][boardCol] = removedPiece;
                        chessBoard[i][j] = pieceToMove;
                    }
                }
            }
        }
        System.out.print("CHECKMATE\n");
        return true;
    }
    
    public static boolean gameWon(ChessPiece[][] chessBoard) {
        return false;
    }

    public void showBoard(ChessPiece[][] chessBoard) {
        char intToChar = 'a';
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (j == 0) {
                    // boardArray[i][j] = String.valueOf(i);
                    System.out.print(String.valueOf(i));
                    //System.out.print(chessBoard[i][j]);
                    System.out.print(" ");
                } else {
                    if (i == 0) {

                        intToChar = (char) (j + 64);
                        System.out.print(" " + intToChar + " ");
                        //boardArray[i][j] = " " + intToChar + " ";
                        //System.out.print(chessBoard[i][j]);

                    } else {

                        System.out.print(chessBoard[i - 1][j - 1].getAbbreviation());
                        System.out.print(" ");

                        //System.out.print(boardArray[i][j]);

                    }

                }
            }
            System.out.print('\n');
        }
        if (!(chessBoard[1][9].getAbbreviation().equals("--"))) {
           System.out.print(chessBoard[1][9].getAbbreviation()+ " is in check!\n");
           
        }
                 
        System.out.print('\n');
    }

}