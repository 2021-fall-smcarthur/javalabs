package com.oreillyauto.w5.d2;

/**
 * ChessPiece abbreviations should be one of the following:
 *   BR - Black Rook        WR - White Rook
 *   BN - Black Knight      WN - White Knight
 *   BB - Black Bishop      WB - White Bishop
 *   BK - Black King        WK - White King
 *   BQ - Black Queen       WQ - White Queen
 *   BP - Black Pawn        WP - White Pawn
 * @author jbrannon5
 */
public class ChessPiece {

    public final static String COLOR_BLACK = "B";
    public final static String COLOR_WHITE = "W";
    public final static String TYPE_KING = "K";
    public final static String TYPE_QUEEN = "Q";
    public final static String TYPE_BISHOP = "B";
    public final static String TYPE_KNIGHT = "N";
    public final static String TYPE_ROOK = "R";
    public final static String TYPE_PAWN = "P"; 
    
    private String type = "";
    private String color = "";
    private boolean moved = false;
    private int enPassante = 0;
    
    public ChessPiece() {
    }

    public ChessPiece(String type, String color, boolean moved) {
        this.type = type;
        this.color = color;
        this.moved = moved;
        enPassante = 0;
    }

    public int getEnPassante() {
        return enPassante;
    }

    public void setEnPassante(int enPassante) {
        this.enPassante = enPassante;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAbbreviation() {
        return this.color + this.type;
    } 
    public boolean getMoved() {
        return this.moved;
    }
    public void setMoved(boolean moved) {
        this.moved = moved;
    }
    public static String[] addX(int n, String arr[], String x)
    {
        int i;
  
        // create a new array of size n+1
        String newarr[] = new String[n + 1];
  
        // insert the elements from
        // the old array into the new array
        // insert all elements till n
        // then insert x at n+1
        for (i = 0; i < n; i++)
            newarr[i] = arr[i];
  
        newarr[n] = x;
  
        return newarr;
    }
  
    //King Moves
    private static String[] findKingMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] moveArray = new String[0];
        for (int i = row-1; i<=row+1;i++) {
            if (i<0 || i>7) {
                continue;
            }for (int j = col-1; j<=col+1; j++) {
                if (j<0 || j>7) {
                    continue;
                }
                if(chessBoard[i][j].getAbbreviation().equals("--")||chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                  
                }
            }
        }
        return moveArray;
    }
    
    //Queen Moves
        private static String[] findQueenMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
            String[] moveArray = new String[0];
            for (int i = row-1; i >= 0; i--) {
                if (chessBoard[i][col].getAbbreviation().equals("--")) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                 }
                else {
                    if (chessBoard[i][col].getColor()!= movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                    }
                    break;
                }
            }
            for (int i = row+1; i <= 7; i++) {
                if (chessBoard[i][col].getAbbreviation().equals("--")) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                 }
                else {
                    if (chessBoard[i][col].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                    }
                    break;
                }
            }
            for (int i = col-1; i >= 0; i--) {
                if (chessBoard[row][i].getAbbreviation().equals("--")) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                 }
                else {
                    if (chessBoard[row][i].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                    }
                    break;
                }
            }
            for (int i = col+1; i <= 7; i++) {
                if (chessBoard[row][i].getAbbreviation().equals("--")) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                 }
                else {
                    if (chessBoard[row][i].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                    }
                    break;
                }
            }
        for (int i = row-1, j = col-1; i >= 0 && j >=0; i--, j--) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row+1, j = col-1; i <= 7 && j >=0; i++, j--) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row-1, j = col+1; i >= 0 && j <=7; i--, j++) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row+1, j = col+1; i <= 7 && j <=7; i++, j++) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
            return moveArray;
    }
        
    //Rook
    private static  String[] findRookMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] moveArray = new String[0];
        for (int i = row-1; i >= 0; i--) {
            if (chessBoard[i][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
             }
            else {
                if (chessBoard[i][col].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                }
                break;
            }
        }
        for (int i = row+1; i <= 7; i++) {
            if (chessBoard[i][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
             }
            else {
                if (chessBoard[i][col].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(col));
                }
                break;
            }
        }
        for (int i = col-1; i >= 0; i--) {
            if (chessBoard[row][i].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
             }
            else {
                if (chessBoard[row][i].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                }
                break;
            }
        }
        for (int i = col+1; i <= 7; i++) {
            if (chessBoard[row][i].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
             }
            else {
                if (chessBoard[row][i].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row)+String.valueOf(i));
                }
                break;
            }
        }
        return moveArray;
    }
    
    //Knight
    private static  String[] findKnightMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] moveArray = new String[0];
        if (row >= 2) {
            if ((col) < 6) {
                if (chessBoard[row - 2][col + 1].getAbbreviation().equals("--") || chessBoard[row - 2][col + 1].getColor()!=movingPiece.getColor() ){
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row - 2) + String.valueOf(col + 1));
                }
                
            }
            if (col >= 1) {
                if (chessBoard[row - 2][col - 1].getAbbreviation().equals("--") || chessBoard[row - 2][col - 1].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row - 2) + String.valueOf(col - 1));
                }

            }
        }
        if (row < 5) {
            if (col < 6) {
                if (chessBoard[row + 2][col + 1].getAbbreviation().equals("--") || chessBoard[row + 2][col + 1].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row + 2) + String.valueOf(col + 1));
                }
            }
                if (col >= 1) {
                    if (chessBoard[row + 2][col - 1].getAbbreviation().equals("--") || chessBoard[row + 2][col - 1].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(row + 2) + String.valueOf(col - 1));
                    }
                }
            }
        
        if (col < 5) {
            if (row < 6) {
                if (chessBoard[row + 1][col + 2].getAbbreviation().equals("--") || chessBoard[row + 1][col + 2].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row + 1) + String.valueOf(col + 2));
                }
            }
        
                if (row >= 1) {
                    if (chessBoard[row - 1][col + 2].getAbbreviation().equals("--") || chessBoard[row - 1][col + 2].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(row - 1) + String.valueOf(col + 2));
                    }
                }
            
        }
        if (col >= 2) {
            if (row < 6) {
                if (chessBoard[row+1][col-2].getAbbreviation().equals("--") || chessBoard[row + 1][col - 2].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(row + 1) + String.valueOf(col - 2));
                }
            }
               if (row >= 1) {
                     if ("--".equals(chessBoard[row-1][col-2].getAbbreviation()) || chessBoard[row-1][col-2].getColor()!=movingPiece.getColor()) {
                        moveArray = addX(moveArray.length, moveArray, String.valueOf(row - 1) + String.valueOf(col - 2));
                    }
                }
            }
        
        return moveArray;
    }

    //Bishop
    private static String[] findBishopMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] moveArray = new String[0];
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row+1, j = col-1; i <= 7 && j >0; i++, j--) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row-1, j = col+1; i > 0 && j <=7; i--, j++) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        for (int i = row+1, j = col+1; i <= 7 && j <=7; i++, j++) {
            if (chessBoard[i][j].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
             }
            else {
                if (chessBoard[i][j].getColor()!=movingPiece.getColor()) {
                    moveArray = addX(moveArray.length, moveArray, String.valueOf(i)+String.valueOf(j));
                }
                break;
            }
        }
        return moveArray;
    }
    
    //Pawn
    private static  String[] findPawnMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] moveArray = new String[0];
        if (movingPiece.getColor().equals("W")){
            if (chessBoard[row-1][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row-1)+String.valueOf(col));
            }
            if (!movingPiece.getMoved() && chessBoard[row-2][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row-2)+String.valueOf(col));
            }
        }
        else {
            if (chessBoard[row+1][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row+1)+String.valueOf(col));
            }
            if (!movingPiece.getMoved() && chessBoard[row+2][col].getAbbreviation().equals("--")) {
                moveArray = addX(moveArray.length, moveArray, String.valueOf(row+2)+String.valueOf(col));
            }
           
        }
        return moveArray;
    }
    
    /*  private static String[] checkOutOfBoard(String[] moveArray) {
        String[] checkedMoveArray = new String[0];
        int row = 0;
        int col = 0;
        for (int i = 0; i < moveArray.length; i++) {
            row = moveArray[i].charAt(0);
            col = moveArray[i].charAt(1);
            if (row < 8 && col < 8 && row > 0 && col > 0) {
                checkedMoveArray = addX(checkedMoveArray.length, checkedMoveArray, moveArray[i]);
        }    
        }
        return checkedMoveArray;
    }*/
    public String[] findLegalMoves(ChessPiece movingPiece, ChessPiece[][] chessBoard, int row, int col) {
        String[] checkedMoveArray = new String[0];
       
           switch (movingPiece.getType()) {
                case "K":
                    return findKingMoves(movingPiece, chessBoard, row, col);
                case "Q":
                    return findQueenMoves(movingPiece, chessBoard, row, col);
                case "R":
                    return findRookMoves(movingPiece, chessBoard, row, col);
                case "N":
                    return findKnightMoves(movingPiece, chessBoard, row, col);
                case "B":
                    return findBishopMoves(movingPiece, chessBoard, row, col);
                case "P":
                    return findPawnMoves(movingPiece, chessBoard, row, col);
                default:
                    
            }
        return checkedMoveArray;
    }

}