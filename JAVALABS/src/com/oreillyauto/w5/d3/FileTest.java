package com.oreillyauto.w5.d3;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileTest {
    public FileTest() {
        // The default classpath is the actual project. It doesn't matter in what package you're
        // running your code. Since we have numbers.txt at the root of the project, we can access it
        // without any other reference. 
        File file = new File("numbers.txt");
        System.out.println("file.exists (root location): " + (file.exists()));
        // Output: file.exists (root location): true
        FileReader fr = null;
        BufferedReader br = null;
        File file2 = new File(getClass().getResource("/com/oreillyauto/w5/d3/numbers.txt").getFile());   
        System.out.println("file2.exists (package): " + (file2.exists()));
        try {
            File file3 = new File(getClass().getResource("/com/oreillyauto/w5/d3/numbers.txt").getFile());
            
            if (!file3.exists()) {
                System.out.println("file not found... creating file...");
                file.createNewFile();
                System.out.println("file created.");
            }
            
            fr = new FileReader(file3);
            br = new BufferedReader(fr);
            String input;
        
            while ((input = br.readLine()) != null) {               
                // Split a comma separated string that contains double quotes
                // Since the RegEx targets double quotes "between" values, we
                // need to remove the double quote at the start of the entry
                // so that the RegEx executes properly.
                String[] values = input.replaceAll("^\"", "")
                        .split("\"?(,|$)(?=(([^\"]*\"){2})*[^\"]*$) *\"?");
                
                for (String string : values) {
                    System.out.println(string + ".");
                }
            }
            
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (Exception e) {/*do nothing*/}
            try {
                fr.close();
            } catch (Exception e) {/*do nothing*/}}
        }

    
    private void testClassPathResource() {
        File file = new File(getClass().getResource("").getFile());
        System.out.println("file.exists: " + (file.exists()));
    }
    
    public static void main(String[] args) throws IOException {
        FileTest file = new FileTest();
        Scanner scanner = new Scanner(System.in);
       // File file1 = new File ("/numbers.txt");
     
    }
}
