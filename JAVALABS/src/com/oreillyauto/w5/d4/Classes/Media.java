package com.oreillyauto.w5.d4.Classes;



public abstract class Media {
    
    protected String creator = "gOD";
    protected String title = "NA";
    protected String releaseDate = "NA";
    protected Media() {
        
    }
    public Media(String title) {
        super();
        this.title = title;
    }
    public String toString() {
        return this.title;
    }
}
