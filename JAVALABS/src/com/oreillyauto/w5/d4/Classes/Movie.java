package com.oreillyauto.w5.d4.Classes;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.w5.d4.Interface.Digital;

public class Movie extends Media implements Digital{
    private String director ="";
    private double realTime = 0;
    private ArrayList<String>actors = new ArrayList<String>();
    
    
    public Movie() {
        
    }
    public Movie(String director, double realTime, ArrayList<String> actors) {
        this.director = director;
        this.realTime = realTime;
        this.actors = actors;     
    }
}
