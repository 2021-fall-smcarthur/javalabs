package com.oreillyauto.w5.d4.Classes;

import com.oreillyauto.w5.d4.Interface.Digital;

public class TVShow extends Media implements Digital{
    
    public String network;
    public TVShow() {
        
    }
    public TVShow(String title, String network){
        this.title = title;
        this.network = network;
    }
}
