package com.oreillyauto.fastsort;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;

public class FastSort extends Thread{
    public static File file1 = new File("file1.txt");
    public static File file2 = new File("file2.txt");
    public static File file3 = new File("file3.txt");
    public static File fileS1 = new File("file1sorted.txt");
    public static File fileS2 = new File("file2sorted.txt");
    public static File fileS3 = new File("file3sorted.txt");
    FileReader fr = null;
    static FileReader fr1 = null;
    static FileReader fr2 = null;
    static FileReader fr3 = null;
    BufferedReader br = null;
    static BufferedReader br1 = null;
    static BufferedReader br2 = null;
    static BufferedReader br3 = null;
    
    
    
    private static void fileExistTest() {
        System.out.println("file.exists (root location): " + (file1.exists()));   
    }

    
    FastSort() {
    }
    
    public static void populateFiles(File f1, File f2, File f3, int numberOfIntries) {
        try {
            FileWriter myWriter1 = new FileWriter(file1);
            FileWriter myWriter2 = new FileWriter(file2);
            FileWriter myWriter3 = new FileWriter(file3);
            Random rand = new Random();
            int upperBound = 100000;
            for (int i = 0; i < numberOfIntries; i++) {
                myWriter1.write(rand.nextInt(upperBound)+"\n");
                myWriter2.write(rand.nextInt(upperBound)+"\n");
                myWriter3.write(rand.nextInt(upperBound)+"\n");
                
            }
            myWriter1.close();
            myWriter2.close();
            myWriter3.close();
            System.out.println("Successfully wrote to the file.");
          } catch (IOException e) {
            System.out.println("An error occurred writting to the file.");
            e.printStackTrace();
          }
    }
    
    private void createThreads() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 3; i++) {
            FastSort object = new FastSort();
            object.start();
            try {
                object.join();
            }catch (InterruptedException ie) {
                System.out.println("Interrupted Exception!: " + ie.getMessage());
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("Process Time: " + (end - start) + " milliseconds");
        System.out.println("Done.");

    }
    public void run() {
        try {
            long threadFileNumber = Thread.currentThread().getId()-11;
            File unsortedFile = new File("file"+threadFileNumber+".txt");
           FileWriter myWriter = new FileWriter ("file"+threadFileNumber+"sorted.txt");
           fr = new FileReader(unsortedFile);
           br = new BufferedReader(fr);
           String input;
           ArrayList<Integer> fileList = new ArrayList<Integer>();
           while ((input = br.readLine()) != null) {
               fileList.add(Integer.parseInt(input));
           }
           Collections.sort(fileList);
           for (int i = 0; i < fileList.size();i++) {
               myWriter.write(fileList.get(i)+"\n");
           }
           myWriter.close();
        }catch(IOException e) {
            System.out.print(Thread.currentThread()+" error"+ e);
        }
    }
   /*
    public static void sortFiles(File f1, File f2, File f3) {
        try {
            FileWriter myWriter1 = new FileWriter("dist/file1sorted.txt");
            FileWriter myWriter2 = new FileWriter("dist/file2sorted.txt");
            FileWriter myWriter3 = new FileWriter("dist/file3sorted.txt"); 
            
            fr1 = new FileReader(f1);
            fr2 = new FileReader(f2);
            fr3 = new FileReader(f3);
            br1 = new BufferedReader(fr1);
            br2 = new BufferedReader(fr2);
            br3 = new BufferedReader(fr3);
            String input;
            ArrayList<Integer> file1List = new ArrayList<Integer>();
            while ((input = br1.readLine()) != null) {
                file1List.add(Integer.parseInt(input));
            }
            Collections.sort(file1List);
            for (int i = 0; i < file1List.size();i++) {
                myWriter1.write(file1List.get(i)+"\n");
            }
            myWriter1.close();
            myWriter2.close();
            myWriter3.close();
        }catch(IOException e) {
            System.out.println("An error occurred writting to the file.");
            e.printStackTrace();
        }
    }
    */
    public static void resetFiles(File f1, File f2, File f3, File sf1, File sf2, File sf3) {
       try {
           PrintWriter writer1 = new PrintWriter(f1);
           writer1.print("");
           writer1.close();
           PrintWriter writer2 = new PrintWriter(f2);
           writer2.print("");
           writer2.close();
           PrintWriter writer3 = new PrintWriter(f3);
           writer3.print("");
           writer3.close();
           PrintWriter writerS1 = new PrintWriter(sf1);
           writerS1.print("");
           writerS1.close();
           PrintWriter writerS2 = new PrintWriter(sf2);
           writerS2.print("");
           writerS2.close();
           PrintWriter writerS3 = new PrintWriter(sf3);
           writerS3.print("");
           writerS3.close();
       }catch(IOException e) {
           
       }
          }

    public static void main(String[] args) throws IOException {
        FastSort fileSorter = new FastSort();
        resetFiles(file1, file2, file3, fileS1, fileS2, fileS3);
        FastSort.populateFiles(FastSort.file1, FastSort.file2, FastSort.file3, 100000);
        //FastSort.sortFiles(FastSort.file1, FastSort.file2, FastSort.file3);
        
        fileSorter.createThreads();


        
    }
}